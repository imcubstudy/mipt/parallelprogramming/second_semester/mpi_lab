#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

char const FILENAME[] = "1b_single";

int const ISIZE = 1000;
int const JSIZE = 1000;

double GetSeconds(struct timespec time1, struct timespec time0)
{
    double seconds = (double)((long)time1.tv_sec - (long)time0.tv_sec);
    seconds += (double)(time1.tv_nsec - time0.tv_nsec) / 1e9;
    return seconds;
}

int main(int argc, char **argv)
{
    double *a = (double *)malloc(ISIZE * JSIZE * sizeof(*a));

    struct timespec start;
    clock_gettime(CLOCK_REALTIME, &start);

    for (int i = 0; i < ISIZE; ++i) {
        for (int j = 0; j < JSIZE; ++j) {
            a[i * JSIZE + j] = 10 * i + j;
        }
    }

    for (int i = 0; i < ISIZE - 3; ++i) {
        for (int j = 4; j < JSIZE; ++j) {
            a[i * JSIZE + j] = sin(0.00001 * a[(i + 3) * JSIZE + (j - 4)]);
        }
    }

    struct timespec end;
    clock_gettime(CLOCK_REALTIME, &end);

    printf("total time = %lf\n", GetSeconds(end, start));

    FILE *output = fopen(FILENAME, "w");
    for (int i = 0; i < ISIZE; ++i) {
        for (int j = 0; j < JSIZE; ++j) {
            fprintf(output, "%lf ", a[i * JSIZE + j]);
        }
        fprintf(output, "\n");
    }
    fclose(output);

    free(a);
    return 0;
}
