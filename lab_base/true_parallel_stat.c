#include <mpi.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>

char const FILENAME[] = "base_true_parallel";

int const ISIZE = 1000;
int const JSIZE = 1000;
int const STAT_SIZE = 32;

enum MPITags {
    STOP,
    ROW_NUM,
    ROW_VALUE,
};

void Master(int mpiWorldSize, double *time)
{
    double *a = (double *)malloc(ISIZE * JSIZE * sizeof(*a));

    double const startTime = MPI_Wtime();

    int numSlaves = mpiWorldSize - 1;
    for (int i = 0; i < numSlaves; ++i) {
        MPI_Send(&i, 1, MPI_INT, i + 1, ROW_NUM, MPI_COMM_WORLD);
    }
    for (int i = numSlaves; i < ISIZE; ++i) {
        MPI_Status status;

        int rowNum = 0;
        MPI_Recv(&rowNum, 1, MPI_INT, MPI_ANY_SOURCE, ROW_NUM, MPI_COMM_WORLD, &status);
        MPI_Recv(&a[rowNum * JSIZE], JSIZE, MPI_DOUBLE, status.MPI_SOURCE, ROW_VALUE, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);

        MPI_Send(&i, 1, MPI_INT, status.MPI_SOURCE, ROW_NUM, MPI_COMM_WORLD);
    }
    for (int i = 0; i < numSlaves; ++i) {
        MPI_Status status;

        int rowNum = 0;
        MPI_Recv(&rowNum, 1, MPI_INT, MPI_ANY_SOURCE, ROW_NUM, MPI_COMM_WORLD, &status);
        MPI_Recv(&a[rowNum * JSIZE], JSIZE, MPI_DOUBLE, status.MPI_SOURCE, ROW_VALUE, MPI_COMM_WORLD,
            MPI_STATUS_IGNORE);
        MPI_Send(&i, 1, MPI_INT, status.MPI_SOURCE, STOP, MPI_COMM_WORLD);
    }

    double const endTime = MPI_Wtime();
    *time = endTime - startTime;

    free(a);
}

void Slave(int mpiRank, int mpiWorldSize)
{
    const int slaveID = mpiRank - 1;
    const int numSlaves = mpiWorldSize - 1;

    double *rowBuf = (double *)malloc(JSIZE * sizeof(*rowBuf));

    for (;;) {
        MPI_Status status;

        int rowNum = 0;
        MPI_Recv(&rowNum, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if (status.MPI_TAG == STOP) {
            break;
        }

        for (int j = 0; j < JSIZE; ++j) {
            double const initialValue = 10.0 * (double)rowNum + (double)j;
            double const param = 0.00001;
            rowBuf[j] = sin(param * initialValue);
        }

        MPI_Send(&rowNum, 1, MPI_INT, 0, ROW_NUM, MPI_COMM_WORLD);
        MPI_Send(rowBuf, JSIZE, MPI_DOUBLE, 0, ROW_VALUE, MPI_COMM_WORLD);
    }

    free(rowBuf);
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int mpiRank = 0, mpiWorldSize = 0;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpiWorldSize);

    if (mpiWorldSize < 2) {
        assert(!"Invalid world size!");
    }

    double *stat = NULL;
    if (mpiRank == 0) {
        stat = (double *) malloc(STAT_SIZE * (mpiWorldSize + 1) * sizeof(*stat));
    }

    for (int i = 0; i < STAT_SIZE; ++i) {
        for (int tempWorldSize = 2; tempWorldSize <= mpiWorldSize; ++tempWorldSize) {
            if (mpiRank == 0) {
                Master(tempWorldSize, &stat[tempWorldSize * STAT_SIZE + i]);
            } else if (mpiRank < tempWorldSize) {
                Slave(mpiRank, tempWorldSize);
            }
            MPI_Barrier(MPI_COMM_WORLD);
        }
        if (mpiRank == 0) {
            printf("Finished %d\n", i);
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    if (mpiRank == 0) {
        double *avgTime = (double *)malloc((mpiWorldSize + 1) * sizeof(*avgTime));
        for (int i = 0; i <= mpiWorldSize; ++i) {
            avgTime[i] = 0.0F;
        }

        for (int tempWorldSize = 2; tempWorldSize <= mpiWorldSize; ++tempWorldSize) {
            for (int i = 0; i < STAT_SIZE; ++i) {
                avgTime[tempWorldSize] += stat[tempWorldSize * STAT_SIZE + i] / STAT_SIZE;
            }
        }

        for (int tempWorldSize = 2; tempWorldSize <= mpiWorldSize; ++tempWorldSize) {
            printf("avg time for world size of %d is %lf\n", tempWorldSize, avgTime[tempWorldSize]);
        }

        free(avgTime);

        free(stat);
    }

    MPI_Finalize();
    return 0;
}
